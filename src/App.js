import { useState } from "react";

function App() {
    // state
    let [count, addingCount] = useState(0);
    return (
        <>
            <h1>Hello {count}</h1>
            <button onClick={() => addingCount(count + 1)}>Click Me!</button>
        </>
    )
}
export default App;